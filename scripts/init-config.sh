#!/bin/bash

function buildConfigs(){
    if [ ${#configs[@]} -eq 0 ]; then
        configs=(${default_configs[*]})
    fi
}

function initstallConfigs(){
    for config in ${configs[*]}; do
        updateScript="$config_home/configs/$config/update.sh"
        if [ -e $updateScript ]; then
            sudo chmod +x $updateScript
            sudo $updateScript
            echo "update $config config success"
        else
            echo "invalid config: $config"
        fi
    done
}

config_home="$(readlink -f $(dirname $(dirname $0)))" 
default_configs=('docker-registry' 'nfs-client' 'ssh' 'dns-client')
configs=($@)

buildConfigs
initstallConfigs
