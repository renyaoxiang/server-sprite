#!/bin/bash

function buildService(){
    echo $@
    if [ $# -gt 1 ]; then
        services=$@
    fi
}

function initstallServices(){
    for service in $services; do
        if [ -e "$service_home/services/$service/start.sh" ]; then
            chmod +x  "$service_home/services/$service/start.sh"
            sudo $service_home/services/$service/start.sh
        else
            echo "invalid service: $service"
        fi
    done
}

service_home="$(readlink -f $(dirname $(dirname $0)))" 
all_services=('webmin')
services=$all_services

buildService $@
initstallServices
