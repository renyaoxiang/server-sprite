#!/bin/bash
# https://ubuntu.com/server/docs/service-nfs?ref=nelsonantunes.com

script_home=$(dirname $0)
apt install nfs-common
mkdir -p /home/vagrant/share/nfs
script_home=$(dirname $0)
cat $script_home/resource/fstab >> /etc/fstab
mount -a
systemctl daemon-reload
echo 'update nfs-client success'