#!/bin/bash
script_home=$(dirname $0)
sudo su root <<-EOF
    chown root:root $script_home/resource/resolv.conf
    chmod 644 $script_home/resource/resolv.conf
    systemctl disable systemd-resolved
    systemctl stop systemd-resolved
    systemctl restart systemd-networkd
    unlink /etc/resolv.conf
    ln -sf $script_home/resource/resolv.conf /etc/resolv.conf
    echo 'dns-client success'
EOF