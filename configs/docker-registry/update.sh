#!/bin/bash

script_home=$(dirname $0)
cat $script_home/resource/daemon.json > /etc/docker/daemon.json
systemctl daemon-reload
systemctl restart docker
echo 'update docker-registry client config'