#!/bin/bash
script_home=$(dirname $0)
cat $script_home/resource/windows.pub >> /home/vagrant/.ssh/authorized_keys
cat $script_home/resource/id_rsa > /home/vagrant/.ssh/id_rsa 
cat $script_home/resource/id_rsa.pub > /home/vagrant/.ssh/id_rsa.pub
cat $script_home/resource/config > /home/vagrant/.ssh/config 
chown -R vagrant:vagrant /home/vagrant/.ssh
chmod 600 /home/vagrant/.ssh/id_rsa
chmod 644 /home/vagrant/.ssh/id_rsa.pub
chmod 400 /home/vagrant/.ssh/config
sudo systemctl restart ssh.service
echo 'update ssh-client success'